﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Chrome;

namespace ReadingTables
{
    class Program:Base
    {

        static void Main(string[] args)
        {
            Driver = new ChromeDriver();
            Driver.Navigate().GoToUrl("file:///E:/c%23/HTMLTable.html");
            //  Program p = new Program();

            TablePage page = new TablePage();
            //Read the table
            Utilities.ReadTable(page.table);

            //Get the cell from the table
            Console.WriteLine($"The name  {Utilities.ReadCell("Name", 2)} with Email {Utilities.ReadCell("Email", 2)} and Phone {Utilities.ReadCell("Phone", 2)}");

            Console.Read();
            


        }
    }
}
